FROM alpine:3.15 as downloader

ARG RPORT_VERSION=0.8.0
ARG FRONTEND_BUILD=1092
ARG NOVNC_VERSION=1.3.0
ARG ENVPLATE_VERSION=1.0.2

# Package Versions https://pkgs.alpinelinux.org/packages
RUN apk --no-cache add unzip=6.0-r9

WORKDIR /app/

RUN set -e \
    && wget -q "https://github.com/cloudradar-monitoring/rport/releases/download/${RPORT_VERSION}/rportd_${RPORT_VERSION}_Linux_$(uname -m).tar.gz" -O rportd.tar.gz \
    && tar xzf rportd.tar.gz rportd

RUN set -e \
    && wget -q "https://downloads.rport.io/frontend/stable/rport-frontend-stable-${RPORT_VERSION}-build-${FRONTEND_BUILD}.zip" -O frontend.zip \
    && unzip frontend.zip -d ./frontend

RUN set -e \
    && wget -q "https://github.com/novnc/noVNC/archive/refs/tags/v${NOVNC_VERSION}.tar.gz" -O novnc.tar.gz \
    && mkdir novnc \
    && tar xzf novnc.tar.gz -C novnc --strip-components=1

WORKDIR /envplate
RUN set -e \
    && arch="$(uname -m)" \
    && if [ "${arch}" = "aarch64" ]; then release_arch="arm64"; else release_arch=${arch}; fi \
    && release_name="envplate_${ENVPLATE_VERSION}_$(uname -s)_${release_arch}.tar.gz" \
    && wget -q "https://github.com/kreuzwerker/envplate/releases/download/v${ENVPLATE_VERSION}/${release_name}" -O envplate.tar.gz \
    && tar -xf envplate.tar.gz

FROM debian:11

ENV API_DOC_ROOT /var/www/rport-frontend/
ENV SERVER_NOVNC_ROOT /var/www/rport-novnc/

COPY --from=downloader /app/rportd /usr/local/bin/rportd
COPY --chmod=u=rwX,g=rX,o=rX --from=downloader /app/frontend/ "${API_DOC_ROOT}"
COPY --chmod=u=rwX,g=rX,o=rX --from=downloader /app/novnc/ "${SERVER_NOVNC_ROOT}"
COPY --from=downloader /envplate/envplate /usr/local/bin/ep

COPY --chmod=755 entrypoint.sh /entrypoint.sh

# Package Versions https://packages.debian.org/stable/allpackages
RUN set -e \
    && apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    pwgen=2.08-2 \
    apache2-utils=2.4.54-1~deb11u1 \
    unzip=6.0-26 \
    curl=7.74.0-1.3+deb11u1 \
    sqlite3=3.34.1-3 \
    netcat=1.10-46 \
    default-mysql-client=1.0.7 \
    openssl=1.1.1n-0+deb11u3 \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN set -e \
    && useradd -d /var/lib/rport -m -u 1000 -U -r -s /bin/false rport \
    && mkdir -p /etc/rport && chown rport:rport /etc/rport \
    && mkdir -p /var/log/rport && chown rport:rport /var/log/rport

USER rport

COPY --chown=rport:rport rportd.conf.template /etc/rportd.conf.template

VOLUME [ "/var/lib/rport/" ]

EXPOSE 8080
EXPOSE 3000

LABEL rport.server.version=${RPORT_VERSION}
LABEL rport.frontend.version=${RPORT_VERSION}-buile-${FRONTEND_BUILD}
LABEL rport.novnc.version=${NOVNC_VERSION}

ENTRYPOINT [ "sh", "-c", "/entrypoint.sh" ]
