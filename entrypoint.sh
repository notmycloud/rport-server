#!/bin/sh

set -e

func_mysql_conn() {
  mysql \
	--user "${DATABASE_DB_USER:-rport}" \
	--password "${DATABASE_DB_PASSWORD:-}" \
	--host "${DATABASE_DB_HOST:-127.0.0.1}" \
	--database "${DATABASE_DB_NAME:-rport}" \
	"$@"
}

func_setup_mysql() {
  echo "Setting Up MySQL database conenction"
  echo "Creating Users table"
  query=$(cat <<-EOM
	CREATE TABLE IF NOT EXISTS "${API_AUTH_USER_TABLE:-users}" (
	  'username' varchar(150) NOT NULL,
	  'password' varchar(255) NOT NULL,
	  'totp_secret' varchar(150),
	  'two_fa_send_to' varchar(150),
	  'token' char(36) default NULL,
	  UNIQUE KEY 'username' ('username')
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	EOM
  )
  func_mysql_conn "${query}"


  echo "Creating Groups table"
  query=$(cat <<-EOM
	CREATE TABLE IF NOT EXISTS "${API_AUTH_GROUP_TABLE:-groups}" (
	  'username' varchar(150) NOT NULL,
	  'group' varchar(150) NOT NULL,
	  UNIQUE KEY 'username_group' ('username','group')
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	EOM
  )
  func_mysql_conn "${query}"

  echo "Creating Clients table"
  query=$(cat <<-EOM
	CREATE TABLE IF NOT EXISTS "${SERVER_AUTH_TABLE:-clients_auth}" (
	  'id' varchar(100) PRIMARY KEY,
	  'password' varchar(100) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	EOM
  )
  func_mysql_conn "${query}"
  
  ## Hash the admin password if not already
  if [ -z "${API_AUTH_ADMIN_PASSWORD_BCRYPT}" ]
  then
    if [ -z "${API_AUTH_ADMIN_PASSWORD}" ]
    then
      generated_admin_pass=$(openssl rand -hex 9)
      echo "Default Admin Pass is ${generated_admin_pass}"
	else
    generated_admin_pass=$(htpasswd -nbB password "${API_AUTH_ADMIN_PASSWORD}" | cut -d: -f2)
	fi
  fi
  
  ## Create the Admin user
  echo "Creating admin user"
  query=$(cat <<-EOM
	INSERT IGNORE INTO "${API_AUTH_USER_TABLE:-users}" VALUES(
	  "${API_AUTH_ADMIN_USERNAME:-admin}",
	  "${API_AUTH_ADMIN_PASSWORD_BCRYPT:-generated_admin_pass}",
	  '',
	  '',
	  "${API_AUTH_ADMIN_EMAIL}"
	);
	REPLACE INTO "${API_AUTH_GROUP_TABLE:-groups}" VALUES(
	  "${API_AUTH_ADMIN_USERNAME:-admin}",
	  'Administrators'
	);
	EOM
  )
  func_mysql_conn "${query}"
}

func_setup_sqlite3() {
  echo "Setting up SQLite database conenction"
  ## Create the database
  touch "${DATABASE_DB_NAME}"

  echo "Create the Users table"
  sqlite3 "${DATABASE_DB_NAME}" <<-EOM
	CREATE TABLE IF NOT EXISTS "${API_AUTH_USER_TABLE:-users}" (
	  'username' TEXT(150) NOT NULL,
	  'password' TEXT(255) NOT NULL,
	  'totp_secret' TEXT,
	  'token' TEXT(36) DEFAULT NULL,
	  'two_fa_send_to' TEXT(150)
	);
	CREATE UNIQUE INDEX IF NOT EXISTS "main"."username"
	ON "${API_AUTH_USER_TABLE:-users}" (
	  'username' ASC
	);
	EOM

  echo "Create the Groups Table"
  sqlite3 "${DATABASE_DB_NAME}" <<-EOM
	CREATE TABLE IF NOT EXISTS "${API_AUTH_GROUP_TABLE:-groups}" (
	  'username' TEXT(150) NOT NULL,
	  'group' TEXT(150) NOT NULL
	);
	CREATE UNIQUE INDEX IF NOT EXISTS "main"."username_group"
	ON "${API_AUTH_GROUP_TABLE:-groups}" (
	  'username' ASC,
	  'group' ASC
	);
	EOM

  echo "Create the Clients Table"
  sqlite3 "${DATABASE_DB_NAME}" <<-EOM
	CREATE TABLE IF NOT EXISTS "${SERVER_AUTH_TABLE:-clients_auth}" (
	  'id' varchar(100) PRIMARY KEY,
	  'password' varchar(100) NOT NULL
	);
	EOM
  
  ## Hash the admin password if not already
  if [ -z "${API_AUTH_ADMIN_PASSWORD_BCRYPT}" ]
  then
    if [ -z "${API_AUTH_ADMIN_PASSWORD}" ]
    then
      generated_admin_pass=$(openssl rand -hex 9)
      echo "Default Admin Pass is ${generated_admin_pass}"
	else
    generated_admin_pass=$(htpasswd -nbB password "${API_AUTH_ADMIN_PASSWORD}" | cut -d: -f2)
	fi
  fi
  
  ## Create the Admin user
  echo "Creating admin user"
  sqlite3 "${DATABASE_DB_NAME}" <<-EOM
	INSERT OR IGNORE INTO "${API_AUTH_USER_TABLE:-users}" VALUES(
	  "${API_AUTH_ADMIN_USERNAME:-admin}",
	  "${API_AUTH_ADMIN_PASSWORD_BCRYPT:-${generated_admin_pass}}",
	  '',
	  '',
	  "${API_AUTH_ADMIN_EMAIL}"
	);
	INSERT OR REPLACE INTO "${API_AUTH_GROUP_TABLE:-groups}" VALUES(
	  "${API_AUTH_ADMIN_USERNAME:-admin}",
	  'Administrators'
	);
	EOM
}

## Setup Basic Single User Auth

if [ -n "${SERVER_AUTH_USER}" ] && [ -n "${SERVER_AUTH_PASSWORD}" ] && [ -z "${SERVER_AUTH}" ]
then
  echo "Configuring Single User client authentication"
  export SERVER_AUTH="${SERVER_AUTH_USER}:${SERVER_AUTH_PASSWORD}"
fi

if [ -n "${API_AUTH_USER}" ] && [ -n "${API_AUTH_PASSWORD}" ] && [ -z "${API_AUTH}" ]
then
  echo "Configuring Single User server authentication"
  export API_AUTH="${API_AUTH_USER}:${API_AUTH_PASSWORD}"
fi

## Setup Database backed Auth

if [ -z "${DATABASE_DB_TYPE}" ]
then
  echo "INFO: Database not specified, resorting to basic auth"
elif [ "${DATABASE_DB_TYPE}" = "mysql" ]
then
  func_setup_mysql
  export SERVER_AUTH_TABLE="${SERVER_AUTH_TABLE:-clients_auth}"
elif [ "${DATABASE_DB_TYPE}" = "sqlite" ]
then
  DATABASE_DB_NAME="${DATABASE_DB_NAME:-${SERVER_DATA_DIR:-/var/lib/rport}/database.sqlite3}"
  func_setup_sqlite3
  export SERVER_AUTH_TABLE="${SERVER_AUTH_TABLE:-clients_auth}"
else
  echo "WARN: Invalid Database Type [${DATABASE_DB_TYPE}]. Accepted values (mysql/sqlite)."
fi

## Generate Self Signed Tunnel Certificates

if [ -z "${SERVER_TUNNEL_PROXY_CERT_FILE}" ] || [ -z "${SERVER_TUNNEL_PROXY_KEY_FILE}" ]
then
  echo "Configuring Tunnel Proxy Certificates"
  export SERVER_TUNNEL_PROXY_CERT_FILE="${SERVER_DATA_DIR:-/var/lib/rport}/tunnel.crt"
  export SERVER_TUNNEL_PROXY_KEY_FILE="${SERVER_DATA_DIR:-/var/lib/rport}/tunnel.key"
  openssl req -x509 -newkey rsa:4096 -sha256 -nodes \
  -keyout "${SERVER_TUNNEL_PROXY_KEY_FILE}" \
  -out "${SERVER_TUNNEL_PROXY_CERT_FILE}" \
  -days "${SERVER_TUNNEL_PROXY_DAYS:-3650}" \
  -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=tunnel.rport.io"
fi

## Generate configuration

conf_file=/etc/rport/rportd.conf
#if [ ! -f "${conf_file}" ]
if [ -z "${CONFIG_FILE}" ]
then
  echo "Writing configuration"
  mkdir -p "$(dirname ${conf_file})"
  cp -f /etc/rportd.conf.template "${conf_file}"
  ep "${conf_file}"
fi

if /usr/local/bin/rportd --version;then
  true
else
  echo "Unable to start the rport server. Check ${LOGGING_LOG_FILE:-/dev/stdout}"
  exit 1
fi

echo "Starting RPort server"
exec /usr/local/bin/rportd --config "${CONFIG_FILE:-${conf_file}}" "$@"
